# set environment variables (if not already done)
export PYTHON_ROOT=~/Python
export PYSPARK_DRIVER_PYTHON=/home/hdfs/Python/bin/python3
export PYSPARK_PYTHON=/home/hdfs/Python/bin/python3
export LD_LIBRARY_PATH=${PATH}
export PYSPARK_PYTHON=${PYTHON_ROOT}/bin/python3
export SPARK_YARN_USER_ENV="PYSPARK_PYTHON=/home/hdfs/Python/bin/python3"
export PATH=${PYTHON_ROOT}/bin/:$PATH
export QUEUE=default
export LIB_HDFS=$HADOOP_HOME/lib/native             
export LIB_JVM=$JAVA_HOME/jre/lib/amd64/server
export HADOOP_PREFIX=$HADOOP_HOME
${SPARK_HOME}/bin/spark-submit \
--master yarn \
--deploy-mode cluster \
--queue ${QUEUE} \
--num-executors 2 \
--executor-memory 4G \
--archives hdfs:///user/${USER}/Python.zip#Python,hdfs:///user/${USER}/mnist.zip#mnist \
${TFoS_HOME}/examples/mnist/mnist_data_setup.py \
--output mnist/csv2 \
--format csv2

