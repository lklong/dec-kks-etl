package com.dec.kafka.offset.reset;

import com.google.gson.Gson;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.util.Arrays;

public class FFTConsume {

    public static void main(String[] args) throws Exception {
        KafkaOffsetReset.loadConfig("/home/hdfs/soft/IdeaProjects/dec-kks-etl/dec-kafka-offset-reset/src/main/resources");
        KafkaOffsetReset.initConsumer();
        KafkaConsumer<String, String> consumer = KafkaOffsetReset.consumer;

        consumer.subscribe(Arrays.asList("test-dec-rt-dealed-t00101"));
        ConsumerRecords<String, String> records;
        Gson gson = new Gson();
        while (true) {
            records = consumer.poll(1000);
            for (ConsumerRecord<String, String> record : records) {
                String value = record.value();
                System.out.println(record.value());
            }
            Thread.sleep(2000);
        }

    }
}
