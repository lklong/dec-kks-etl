package com.dec.kafka.offset.reset;

import lombok.Data;

import java.util.Map;

/**
 * <p/>
 * <li>Description:KKSEntity kks外层数据解析</li>
 * <li>@author: liukailong <kailong.liu@cdcalabar.com> </li>
 * <li>Date: 18-4-4 上午9:33</li>
 * <li>@version: 2.0.0 </li>
 * <li>@since JDK 1.8 </li>
 */
@Data
public class KKSEntity {
    /**
     * {
     * "power_type": "T",               // 电厂类型
     * "power_code": "001",             // 电厂编码
     * "power_unit_code": "01",         // 机组名称
     * "collect_time": 1517450744666
     * "packeted_data": {                    // 8000个测点数据
     * "测点标准编码1": {                // 单个测点包数据
     *          "src_code":"测点原始编码1",   // 测点原始编码
     *          "substd_code": "xxxx"         // 子系统编码
     *          "src_desc": "xxxx",           // 测点描述
     *          "src_value": 21.931957,       // 原始值
     *          "src_time": 1517450744000,    // 原始时间
     *          "src_data_quality": 192,      // 原始质量
     *          "std_code": "xxxx",           // 标准编码
     *          "std_time": 15174507446666,   // 校准时间
     *          "std_value": 20.76,           // 校准值
     *          "std_data_quality": 0,        // 校准质量
     *          "set_code": "xxxxx"           // 机组编码
     *        }
     *     }
     * }
     */
    private String power_type;
    private String power_code;
    private String power_unit_code;
    private int collect_time;
    private Map<String, PacketData> packeted_data;

}
