package com.dec.kafka.offset.reset;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.TopicPartition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class ResetMain {
    private static final Logger LOGGER = LoggerFactory.getLogger(ResetMain.class);

    private static String bootstrap_servers = "kafka001:6667,kafka002:6667,kafka003:6667,kafka004:6667,kafka005:6667";
    //    private static String bootstrap_servers = "bigdata.lkl.com:9092, bigdata.lkl.com:9093, bigdata.lkl.com:9094";
    private static String offset_reset_config = "earliest";

    public static void main(String[] args) throws InterruptedException {

        String topic = args[0];
        String groupId = args[1];
        int partitionId = Integer.parseInt(args[2]);
        int offset = Integer.parseInt(args[3]);
        LOGGER.info("修复对象：主题{}", topic);
        LOGGER.info("修复对象：消费組ID{}", groupId);
        LOGGER.info("修复对象：分区ID{}", partitionId);
        LOGGER.info("修复对象：偏移量{}", offset);

        try {
            LOGGER.info("修复开始：主题{}，消费組ID{}，分区ID{},偏移量{}", topic, groupId, partitionId, offset);

            KafkaConsumer<String, String> consumer = initConsumer(offset_reset_config, groupId, bootstrap_servers);
            reset(consumer, topic, groupId, partitionId, offset);

            LOGGER.info("修复完成：主题{}，消费組ID{}，分区ID{},偏移量{}", topic, groupId, partitionId, offset);
        } catch (Exception e) {
            LOGGER.error("修复失败：主题{}，消费組ID{}，分区ID{},偏移量{}", topic, groupId, partitionId, offset);
            LOGGER.error("修复失败", e);
        }

    }

    public static void reset(KafkaConsumer<String, String> consumer,
                             String topic,
                             String group_id,
                             int partition,
                             int offset) throws InterruptedException {
        Map<TopicPartition, OffsetAndMetadata> offsetAndMetadataMap = new HashMap<TopicPartition, OffsetAndMetadata>();
        TopicPartition tp = new TopicPartition(topic, partition);
        OffsetAndMetadata om = new OffsetAndMetadata(offset, group_id);
        offsetAndMetadataMap.put(tp, om);
        consumer.commitSync(offsetAndMetadataMap);
    }

    public static KafkaConsumer<String, String> initConsumer(String offset_reset_config,
                                                             String group_id,
                                                             String bootstrap_servers) {
        Properties config = new Properties();
        config.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, offset_reset_config);
        config.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, 1);
        config.put(ConsumerConfig.GROUP_ID_CONFIG, group_id);
        config.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrap_servers);
        config.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        config.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        config.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true);

        KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(config);

        return consumer;
    }

//    public static void consume( KafkaConsumer<String, String> consumer,String topic) throws InterruptedException {
//        consumer.subscribe(Arrays.asList(topic));
//        ConsumerRecords<String, String> records;
//        Gson gson = new Gson();
//        while (true) {
//            records = consumer.poll(1000);
//            for (ConsumerRecord<String, String> record : records) {
//                String value = record.value();
//                KKSEntity kks = gson.fromJson(value, KKSEntity.class);
//                long data_time = kks.getCollect_time();
//                LOGGER.warn("当前偏移量：" + record.offset() + "，寻找偏移量->" + DateFormatUtils.format(data_time * 1000, "yyyy-MM-dd HH:mm:ss"));

//                if (Long.valueOf(data_time * 1000).compareTo(start_date_time) == 1) {
//                    LOGGER.error("找到起始偏移量：" + record.offset() + "，当前数据时间：" + kks.getCollect_time() + "==>" + DateFormatUtils.format(data_time * 1000, "yyyy-MM-dd HH:mm:ss"));
//                    offset = (int) record.offset();
//                    reset();
//                    System.exit(1);
//                } else {
//                    LOGGER.warn("当前偏移量：" + record.offset() + "，寻找偏移量->" + DateFormatUtils.format(data_time * 1000, "yyyy-MM-dd HH:mm:ss"));
//                }
//                consumer.commitSync();
//            }
//            Thread.sleep(2000);
//        }
//
//    }
}
