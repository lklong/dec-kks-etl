package com.dec.exception;

//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;

import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.*;

public class ThreadPoolExceptionHandler {

    //    private final static Logger LOGGER = LogManager.getLogger(ThreadPoolExceptionHandler.class);
    private final static Logger LOGGER = LoggerFactory.getLogger(ThreadPoolExceptionHandler.class);

    public static void main(String[] args) {
        int corePoolSize = 5;
        int maxPoolSize = 6;
        int taskQueueSize = 100;
        LOGGER.info("线程池初始化！");
        LOGGER.warn("线程池初始化！");

        ThreadFactory executorThreadFactory = new BasicThreadFactory.Builder()
                .namingPattern("task-scanner-executor-%d")
                .uncaughtExceptionHandler(new GlobalExceptionResolver())
                .build();

        ExecutorService service = new ThreadPoolExecutor(corePoolSize, maxPoolSize, 1000000,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<Runnable>(taskQueueSize),
                executorThreadFactory,
                new RejectedExecutionHandler() {
                    @Override
                    public void rejectedExecution(Runnable runnable, ThreadPoolExecutor threadPoolExecutor) {
                        try {
                            threadPoolExecutor.getQueue().put(runnable);
                        } catch (InterruptedException e) {
                            LOGGER.error("重新加入任务队列！", e);
                        }
                    }
                });

        for (int i = 0; i < 3; i++) {
            Future<?> tasks = service.submit(new ThreadWorker1());
        }
    }


}
