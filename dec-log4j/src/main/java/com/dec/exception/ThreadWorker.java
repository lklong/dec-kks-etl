package com.dec.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ThreadWorker extends Thread {
    //    private final static org.apache.logging.log4j.Logger LOGGER = LogManager.getLogger(ThreadWorker.class);
    private final static Logger LOGGER = LoggerFactory.getLogger(ThreadPoolExceptionHandler.class);

    @Override
    public void run() {
        try {
            throw new Exception("异常测试！");
        } catch (Exception e) {
            LOGGER.error("异常信息：{}", e);
        }

    }
}
