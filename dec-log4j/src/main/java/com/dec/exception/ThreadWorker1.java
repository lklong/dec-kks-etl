package com.dec.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ThreadWorker1 extends Thread {
    private final static Logger LOGGER = LoggerFactory.getLogger(ThreadWorker1.class);

    @Override
    public void run() {
        doSomeThing();
    }

    private void doSomeThing() {
        try {
            int i = 0;
            while (true) {
                LOGGER.info("当前线程：{}，值：{}", Thread.currentThread().getId(), i);
                if (i++ == 10) {
                    throw new Exception("线程超载！");
                }
                Thread.sleep(1000);
            }
        } catch (Exception e) {
            LOGGER.error("当前线程：{}，异常信息：{}", Thread.currentThread().getId(), e);
            System.exit(1);
        }
    }
}
