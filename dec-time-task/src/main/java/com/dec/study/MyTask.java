package com.dec.study;

import java.util.Date;

public class MyTask extends Thread {

    @Override
    public void run() {
        System.out.println("my task 开始执行-----" + new Date());
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("my task 结束执行-----" + new Date());
    }
}
