package com.dec.study;

import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class MyTimerService {

    public static void main(String[] args) {
        ScheduledExecutorService service = Executors.newScheduledThreadPool(10);
        Thread task = new MyTask();
        System.out.println(new Date());
        service.scheduleAtFixedRate(task, 10, 5, TimeUnit.SECONDS);
    }

}
