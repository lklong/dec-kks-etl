package com.dec.turbine.data.receiver;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerAdapter;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;

/**
 * @author hdfs
 */
public class MyServerHandler extends ChannelHandlerAdapter {

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg){
        ByteBuf byteBuf = (ByteBuf) msg;
		int bn = byteBuf.readableBytes();
        System.out.println("数据大小："+bn);
		byte[] bytes = new byte[bn];
        ByteBuf msg1 = Unpooled.copiedBuffer(bytes);
		ctx.write(msg1);
	}

    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
        super.write(ctx, msg, promise);
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx){
        ctx.flush();
    }

    @Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable t){
		ctx.close();
	}
 
}