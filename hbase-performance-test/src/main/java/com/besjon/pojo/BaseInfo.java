/**
  * Copyright 2018 bejson.com 
  */
package com.besjon.pojo;

import lombok.Data;

/**
 * Auto-generated: 2018-09-12 13:42:15
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
@Data
public class BaseInfo {

    private int work_identity;
    private int sample_points_per_cycle;
    private int sampling_period;
    private int sampling_number;
    private int rotate_speed;
    private int fast_variable_channels;


}