/**
  * Copyright 2018 bejson.com 
  */
package com.besjon.pojo;

import lombok.Data;

/**
 * Auto-generated: 2018-09-12 13:42:15
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
@Data
public class Channel {

    private int peak;
    private int phase_1x;
    private int amplitude_1x;
    private int phase_2x;
    private int amplitude_2x;
    private int amplitude_half;
    private int voltage;

}