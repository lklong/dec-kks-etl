/**
  * Copyright 2018 bejson.com 
  */
package com.besjon.pojo;

import lombok.Data;

/**
 * Auto-generated: 2018-09-12 13:42:15
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
@Data
public class Header {

    private int head;
    private String plant_code;
    private String set_code;
    private int device_type;
    private long time;
    private int data_length;

}