package com.besjon.pojo;

import com.google.gson.Gson;

public class Test {

    static String json = "{\n" +
            "    \"header\": {\n" +
            "        \"head\": 1.0,\n" +
            "        \"plant_code\": \"T001\",\n" +
            "        \"set_code\": \"01\",\n" +
            "        \"device_type\": 1,\n" +
            "        \"time\": 135878999999,\n" +
            "        \"data_length\": 4999\n" +
            "    },\n" +
            "    \"base_info\": {\n" +
            "        \"work_identity\": 1.0,\n" +
            "        \"sample_points_per_cycle\": 1024,\n" +
            "        \"sampling_period\": 8,\n" +
            "        \"sampling_number\": 1024,\n" +
            "        \"rotate_speed\": 1024,\n" +
            "        \"fast_variable_channels\": 24\n" +
            "    },\n" +
            "    \"channel\": {\n" +
            "        \"0000000007852\": {\n" +
            "            \"peak\": 1.0,\n" +
            "            \"phase_1x\": 1.0,\n" +
            "            \"amplitude_1x\": 1.0,\n" +
            "            \"phase_2x\": 1.0,\n" +
            "            \"amplitude_2x\": 1.0,\n" +
            "            \"amplitude_half\": 1.0,\n" +
            "            \"voltage\": 1.0,\n" +
            "            \"waveform_data\": [\n" +
            "                123,\n" +
            "                123\n" +
            "            ]\n" +
            "        },\n" +
            "        \"0000000007853\": {\n" +
            "            \"peak\": 1.0,\n" +
            "            \"phase_1x\": 1.0,\n" +
            "            \"amplitude_1x\": 1.0,\n" +
            "            \"phase_2x\": 1.0,\n" +
            "            \"amplitude_2x\": 1.0,\n" +
            "            \"amplitude_half\": 1.0,\n" +
            "            \"voltage\": 1.0,\n" +
            "            \"waveform_data\": [\n" +
            "                123,\n" +
            "                123\n" +
            "            ]\n" +
            "        }\n" +
            "    }\n" +
            "}";
    public static void main(String[] args) {
        Gson gson = new Gson();
        System.out.println(gson.fromJson(json,VibrationEntry.class));

    }
}
