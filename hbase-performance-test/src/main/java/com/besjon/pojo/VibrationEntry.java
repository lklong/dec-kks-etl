/**
  * Copyright 2018 bejson.com 
  */
package com.besjon.pojo;

import lombok.Data;

import java.util.Map;

/**
 * Auto-generated: 2018-09-12 13:42:15
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
@Data
public class VibrationEntry {

    private Header header;
    private BaseInfo base_info;
    private Map<String,Channel> channel;

}