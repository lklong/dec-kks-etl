package com.calabar.hbase.pf.test;

import lombok.Data;

@Data
public class HBaseEntry {

    /**rowkey子系统编码后六位+时间戳（单位：s），总共16位*/
    private String rowKey;
    /**原厂编码*/
    private String src="10KJAAJAXMDEGG.PV";
    /**子系统编码*/
    private String sutd;
    /**opc时间戳*/
    private String srt="OPC0123456";
    /**原厂数据质量*/
    private String srdq="123";
    /**原始值*/
    private String srv="59999.45";
    /**系统编码*/
    private String std;
    /**采集时间戳*/
    private String stt="CJ01234567";
    /**校准数据质量*/
    private String sdq="123";
    /**校准值*/
    private String stv="123";
    /**机组编码*/
    private String sc="01";
    /**电厂编码*/
    private String pc="T001";

    public static HBaseEntry generateEntry(){
        Long ts = System.currentTimeMillis()/1000;
        String sutd = "000000000001";
        HBaseEntry entry = new HBaseEntry();
//        entry.setRowKey();

        return null;
    }

}
