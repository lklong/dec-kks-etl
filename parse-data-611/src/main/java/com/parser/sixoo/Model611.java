package com.parser.sixoo;

import lombok.Data;

import java.util.List;

@Data
public class Model611 {
    private Double Score;
    private List<String> members;

    public Model611(Double Score, List<String> members) {
        this.Score = Score;
        this.members = members;
    }
}
