package com.parser.sixoo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingDeque;

public class Parser611T {

    private final static Logger LOGGER = LoggerFactory.getLogger(Parser611T.class);

    private final static Queue<ByteBuffer> queueA = new LinkedBlockingDeque<>();
    private final static Queue<ByteBuffer> queueB = new LinkedBlockingDeque<>();

    private static final String[] ids = new String[30000];
    private static final Object[] values = new Object[15000];

    private static void genIDS() {
        String p1 = "p0000";
        String p2 = "p000";
        String p3 = "p00";
        String p4 = "p0";
        String p5 = "p";
        for (int i = 1; i <= ids.length; i++) {
            if (i < 10) {
                ids[i-1] = p1 + i;
            } else if (i >= 10 && i < 100) {
                ids[i-1] = p2 + i;
            } else if (i >= 100 && i < 1000) {
                ids[i-1] = p3 + i;
            } else if (i >= 1000 && i < 10000) {
                ids[i-1] = p4 + i;
            } else if (i >= 10000 && i < 100000) {
                ids[i-1] = p5 + i;
            }
        }
    }


    /**
     * Mapped File way MappedByteBuffer 可以在处理大文件时，提升性能
     *
     * @param filename
     * @return
     * @throws IOException
     */
    public static byte[] toByteArrayforBigFile(String filename) throws IOException {

        FileChannel fc = null;
        try {
            fc = new RandomAccessFile(filename, "r").getChannel();
            MappedByteBuffer byteBuffer = fc.map(FileChannel.MapMode.READ_ONLY, 0,
                    fc.size()).load();
            System.out.println(byteBuffer.isLoaded());
            byte[] result = new byte[(int) fc.size()];
            int j = 0;
            if (byteBuffer.remaining() > 0) {
                byteBuffer.get(result, 0, byteBuffer.remaining());
            }
            return result;
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } finally {
            try {
                fc.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }



    public static void main(String[] args) throws IOException {
        byte[] bytes = toByteArrayforBigFile("/home/hdfs/soft/IdeaProjects/dec-kks-etl/parse-data-611/data/1.dat");

        genIDS();

        System.out.println(bytes.length);
        long start = System.currentTimeMillis();

        ByteBuffer buf = ByteBuffer.wrap(bytes);

        // 文件头
        byte[] pack_info = new byte[512];
        buf.get(pack_info, 0, 512);
        while (buf.hasRemaining()) {
            // 包头解析
            byte[] pack_head = new byte[12];
            buf.get(pack_head, 0, 12);
            ByteBuffer buf_head = ByteBuffer.wrap(pack_head);
            byte[] byte_channel_1 = new byte[2];
            buf_head.get(byte_channel_1, 0, 2);
            byte[] byte_channel_2 = new byte[2];
            buf_head.get(byte_channel_2, 0, 2);
            byte[] byte_channel_flag = new byte[2];
            buf_head.get(byte_channel_flag, 0, 2);
            byte[] byte_data_len = new byte[2];
            buf_head.get(byte_data_len, 0, 2);
            byte[] byte_data_time = new byte[4];
            buf_head.get(byte_data_time, 0, 4);
            // 包体解析
            byte[] pack_data = new byte[13500];
            buf.get(pack_data, 0, 13500);

            ByteBuffer pd_bf = ByteBuffer.wrap(pack_data);
            byte[] pv = new byte[4];
            int j = 0;
            while (pd_bf.hasRemaining()) {
                pd_bf.get(pv, 0, 4);
                if (j > 3000) {
                    for (int k = 0; k < 12000; k++) {
                        values[j] = 1;
                        j++;
                    }
//                    values[j] = ByteUtil611.byte2intWithB(pv);
                } else {
                    values[j] = ByteUtil611.byte2floatWithB(pv);
                    j++;
                }
            }
        }
        long end = System.currentTimeMillis();
        System.out.println("耗时：" + (end - start));
    }

    public static String parseByte2HexStr(byte buf[]) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < buf.length; i++) {
            String hex = Integer.toHexString(buf[i] & 0xFF);
            if (hex.length() == 1) {
                hex = '0' + hex;
            }
            sb.append(hex.toUpperCase());
        }
        return sb.toString();
    }

}
