package com.second.sixoo;


import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

public class GZIPcompress {

    static int length =Integer.MAX_VALUE;

    public static void main(String[] args) throws Exception {
        long start = System.currentTimeMillis();
        //注意，文件通道的可读可写要建立在文件流本身可读写的基础之上
        FileChannel fc = new RandomAccessFile("fly2.txt", "rw").getChannel();
        MappedByteBuffer out = fc.map(FileChannel.MapMode.READ_WRITE, 0, length);

        String line = "";
        int k = 0;
        out.put("channel".getBytes());
        out.put(",time".getBytes());
        for (int i = 1; i < 15003; i++) {
            out.put((",p"+i).getBytes());
        }
        out.put("\n".getBytes());
        for (int i = 56643; i < 120000; i++) {
            try {
                long s1 = System.currentTimeMillis();
                line = "a," + (start + i);
                out.put(line.getBytes());
                for (int j = 1; j < 15002; j++) {
                    if(j>3000){
                        out.put(",1".getBytes());
                    }else {
                        out.put((","+j).getBytes());
                    }
                }
                out.put("\n".getBytes());
                long e1 = System.currentTimeMillis();
                if (i % 100 == 0) {
                    System.out.println("i=>" + i + "," + (e1 - s1));
                }
            } catch (Exception e) {
//                k++;
//                out = fc.map(FileChannel.MapMode.READ_WRITE, length*k, length);
//                System.out.println("重新计算偏移位置："+(length*k));
                throw new Exception("",e);
            }

        }
        System.out.println("Finished writing!" + (System.currentTimeMillis() - start));
        fc.close();


        //做准备压缩一个字符文件，注，这里的字符文件要是GBK编码方式的
//        BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(
//                "e:/tmp/source.txt"), "GBK"));
//        //使用GZIPOutputStream包装OutputStream流，使其具体压缩特性，最后会生成test.txt.gz压缩包
//        //并且里面有一个名为test.txt的文件
//        BufferedOutputStream out = new BufferedOutputStream(new GZIPOutputStream(
//                new FileOutputStream("test.txt.gz")));
//        System.out.println("开始写压缩文件...");
//        int c;
//        while ((c = in.read()) != -1) {
//
//            /*
//             * 注，这里是压缩一个字符文件，前面是以字符流来读的，不能直接存入c，因为c已是Unicode
//             * 码，这样会丢掉信息的（当然本身编码格式就不对），所以这里要以GBK来解后再存入。
//             */
//            out.write(String.valueOf((char) c).getBytes("GBK"));
//        }
//        in.close();
//        out.close();
//        System.out.println("开始读压缩文件...");
//        //使用GZIPInputStream包装InputStream流，使其具有解压特性
//        BufferedReader in2 = new BufferedReader(new InputStreamReader(
//                new GZIPInputStream(new FileInputStream("test.txt.gz")), "GBK"));
//        String s;
//        //读取压缩文件里的内容
//        while ((s = in2.readLine()) != null) {
//            System.out.println(s);
//        }
//        in2.close();
    }
}