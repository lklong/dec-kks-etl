package com.second.sixoo;

import org.apache.hadoop.hbase.util.Bytes;

import java.util.*;

public class TestListCap {

    public static void main(String[] args) throws InterruptedException {
        List<Object[]> list = new ArrayList<Object[]>(120000);
        System.out.println(System.currentTimeMillis());
        Object[] obj;
        System.out.println(System.currentTimeMillis());
        char ch = 'a';
        for (int i = 0; i < 120000; i++) {
            if(i>60000){
                ch='b';
            }
            obj = new Object[15002];
            for (int j = 0; j <15002 ; j++) {
                if(j==0) {
                    obj[0] = ch;
                }else if(j==1){
                    obj[j]=System.currentTimeMillis();
                }else if(j>3000){
                    obj[j] ='1';
                }else {
                    obj[j]=j;
                }
            }
            list.add(obj);
            if(i%1000==0){
                System.out.println(i);
            }
        }
        System.out.println(System.currentTimeMillis());
        Set<Object> set = new HashSet<Object>();
        for (int i = 0; i < list.size(); i++) {
            set.add(list.get(i)[0]);
        }

        System.out.println(System.currentTimeMillis()+":"+ Arrays.toString(set.toArray()));
        Thread.sleep(100000);
    }

    public static void main2(String[] args) {
        System.out.println(Bytes.toBytes(System.currentTimeMillis()+"").length);
        System.out.println(Bytes.toBytes(System.currentTimeMillis()).length);
    }
}
