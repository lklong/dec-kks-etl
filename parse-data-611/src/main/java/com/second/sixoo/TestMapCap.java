package com.second.sixoo;

import org.apache.hadoop.hbase.util.Bytes;

import java.util.HashMap;
import java.util.Map;

public class TestMapCap {

    public static void main(String[] args) throws InterruptedException {
        Map<String,Object[]> map = new HashMap<>(30000);
        for (int i = 0; i < 30000; i++) {
            String key = "p"+i;
            Object[] obj = new Object[60000];
            for (int j = 0; j < 60000; j++) {
                obj[j] = j;
                if(j>0&j%1000==0){
                    System.out.println(i+">>>>>>>>"+j);
                }
            }
            map.put(key,obj);
        }

        Thread.sleep(100000);
    }

    public static void main2(String[] args) {
        System.out.println(Bytes.toBytes(System.currentTimeMillis()+"").length);
        System.out.println(Bytes.toBytes(System.currentTimeMillis()).length);
    }
}
