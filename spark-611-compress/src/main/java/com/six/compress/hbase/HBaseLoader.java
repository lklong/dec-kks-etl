package com.six.compress.hbase;

import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.util.Bytes;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class HBaseLoader {

    public static void main(String[] args) throws Exception {
//        HBaseUtils.createTable("fly2", "f");
//        HBaseUtils.truncateTable("fly1");
        byte[] cf = "f".getBytes();
        long start = System.currentTimeMillis();
        Random random = new Random();
        List<Put> puts = new ArrayList<>();
        Put put = null;
        for (int k = 0; k < 80000; k++) {
            put = new Put(Bytes.toBytes(start + k));
            for (int i = 0; i < 120; i++) {
                put.addColumn(cf, String.valueOf("p" + i).getBytes(), Bytes.toBytes(random.nextFloat()));
            }
            for (int i = 120; i < 680; i++) {
                put.addColumn(cf, String.valueOf("p" + i).getBytes(), Bytes.toBytes(true));
            }
            puts.add(put);
            System.out.println("---------------"+k);
            if ((k + 1) % 1 == 0) {
                HBaseUtils.batchPuts2("fly2", puts);
                long end = System.currentTimeMillis();
                System.out.println("cur=>" + k + "  avg=>" + (end - start) / k + "ms/条");
                puts.clear();
            }
        }

        if (puts.size() > 0) {
            HBaseUtils.batchPut("fly", puts);
        }
        System.out.println("success over----------------");

        HBaseUtils.closeConn();
    }


}
