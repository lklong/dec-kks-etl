package com.six.compress.old;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class ByteUtil611 {


    public static char byte2charWithB(byte[] data) {
        ByteBuffer bb = ByteBuffer.wrap(data, 0, 1);
        bb.order(ByteOrder.BIG_ENDIAN);
        char s = bb.getChar();
        return s;
    }

    public static char byte2charWithL(byte[] data) {
        ByteBuffer bb = ByteBuffer.wrap(data, 0, 1);
        bb.order(ByteOrder.LITTLE_ENDIAN);
        char s = bb.getChar();
        return s;
    }

    public static short byte2shortWithB(byte[] data) {
        ByteBuffer bb = ByteBuffer.wrap(data, 0, 2);
        bb.order(ByteOrder.BIG_ENDIAN);
        short s = bb.getShort();
        return s;
    }

    public static short byte2shortWithL(byte[] data) {
        ByteBuffer bb = ByteBuffer.wrap(data, 0, 2);
        bb.order(ByteOrder.LITTLE_ENDIAN);
        short s = bb.getShort();
        return s;
    }

    public static int byte2intWithB(byte[] data) {
        ByteBuffer bb = ByteBuffer.wrap(data, 0, 4);
        bb.order(ByteOrder.BIG_ENDIAN);
        int s = bb.getInt();
        return s;
    }

    public static int byte2intWithL(byte[] data) {
        ByteBuffer bb = ByteBuffer.wrap(data, 0, 4);
        bb.order(ByteOrder.LITTLE_ENDIAN);
        int s = bb.getInt();
        return s;
    }

    public static long byte2longWithB(byte[] data) {
        ByteBuffer bb = ByteBuffer.wrap(data, 0, 8);
        bb.order(ByteOrder.BIG_ENDIAN);
        long s = bb.getLong();
        return s;
    }

    public static long byte2longWithL(byte[] data) {
        ByteBuffer bb = ByteBuffer.wrap(data, 0, 8);
        bb.order(ByteOrder.LITTLE_ENDIAN);
        long s = bb.getLong();
        return s;
    }

    public static float byte2floatWithB(byte[] data) {
        ByteBuffer bb = ByteBuffer.wrap(data, 0, 4);
        bb.order(ByteOrder.BIG_ENDIAN);
        float s = bb.getFloat();
        return s;
    }

    public static float byte2floatWithL(byte[] data) {
        ByteBuffer bb = ByteBuffer.wrap(data, 0, 4);
        bb.order(ByteOrder.LITTLE_ENDIAN);
        float s = bb.getFloat();
        return s;
    }

    public static double byte2doubleWithB(byte[] data) {
        ByteBuffer bb = ByteBuffer.wrap(data, 0, 8);
        bb.order(ByteOrder.BIG_ENDIAN);
        double s = bb.getDouble();
        return s;
    }

    public static double byte2doubleWithL(byte[] data) {
        ByteBuffer bb = ByteBuffer.wrap(data, 0, 8);
        bb.order(ByteOrder.LITTLE_ENDIAN);
        double s = bb.getDouble();
        return s;
    }

    public static byte[] short2byteWithB(short s) {
        ByteBuffer bb = ByteBuffer.allocate(2).putShort(s).order(ByteOrder.BIG_ENDIAN);
        byte[] b = bb.array();
        return b;
    }

    public static byte[] int2byteWithB(int s) {
        ByteBuffer bb = ByteBuffer.allocate(4).putInt(s).order(ByteOrder.BIG_ENDIAN);
        byte[] b = bb.array();
        return b;
    }

    public static byte[] long2byteWithL(long s) {
        ByteBuffer bb = ByteBuffer.allocate(8).putLong(s).order(ByteOrder.LITTLE_ENDIAN);
        byte[] b = bb.array();
        return b;
    }

    public static byte[] float2byteWithL(float s) {
        ByteBuffer bb = ByteBuffer.allocate(4).putFloat(s).order(ByteOrder.LITTLE_ENDIAN);
        byte[] b = bb.array();
        return b;
    }
    public static byte[] double2byteWithL(double s) {
        ByteBuffer bb = ByteBuffer.allocate(8).putDouble(s).order(ByteOrder.LITTLE_ENDIAN);
        byte[] b = bb.array();
        return b;
    }
    public static String conver2HexStr(byte[] b) {
        StringBuffer result = new StringBuffer();
        for (int i = b.length - 1; i >= 0; i--) {
            result.append(Integer.toString(b[i] & 0xff, 2) + ",");
        }
        return result.toString().substring(0, result.length() - 1);
    }

    public static void main(String[] args) {
        byte[] b = {52, -68, 51, 43};
        System.out.println(conver2HexStr(b));

//        short s = -3;
//        byte[] b = short2byteWithB(s);
//        System.out.println(byte2shortWithB(b));
//        203617808
//        long start = System.currentTimeMillis();
//        for (int i = 0; i < 10000000; i++) {
//            byte[] b = {52, -68};
//            byte2intWithB(b);
//        }
//        long end = System.currentTimeMillis();
//        System.out.println(end - start);
//        byte[] b = new byte[2];
//        int c = 0xc22f610;
//        System.out.println(c);
//        System.out.println(Short.MAX_VALUE);

    }
}
