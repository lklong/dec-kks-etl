package com.six.compress.old;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.orc.CompressionKind;
import org.apache.orc.OrcFile;
import org.apache.orc.TypeDescription;
import org.apache.orc.Writer;
import org.apache.orc.storage.ql.exec.vector.BytesColumnVector;
import org.apache.orc.storage.ql.exec.vector.LongColumnVector;
import org.apache.orc.storage.ql.exec.vector.VectorizedRowBatch;

import java.util.Random;

@SuppressWarnings("all")
public class ORCWriterByteTest {

    public static void main(String[] args) throws Exception {

        //定义ORC数据结构，即表结构
        TypeDescription schema = TypeDescription.createStruct();
        schema.addField("channel", TypeDescription.createString());
        schema.addField("time", TypeDescription.createLong());
        schema.addField("p1", TypeDescription.createByte());
//        schema.addField("p2", TypeDescription.createChar());
        Writer writer = null;
        try {
            //输出ORC文件本地绝对路径
            String lxw_orc1_file = "/home/hdfs/data/fly_param_2_" + System.currentTimeMillis() + ".orc";
            Configuration conf = new Configuration();
            FileSystem.getLocal(conf);
            writer = OrcFile.createWriter(
                    new Path(lxw_orc1_file),
                    OrcFile.writerOptions(conf)
                            .setSchema(schema)
//                            .stripeSize(1024)
                            .bufferSize(1024)
//                            .blockSize(1024)
                            .compress(CompressionKind.SNAPPY)
                            .version(OrcFile.Version.V_0_12)
            );

            VectorizedRowBatch batch = schema.createRowBatch(10000);
            long time = System.currentTimeMillis();
            Random random = new Random(10000);
            for (int i = 0; i < 10000; i++) {
                System.out.println("aaaaaaaaaaaaaaaaa=>" + i);
                int rowcount = batch.size++;
                ((BytesColumnVector) batch.cols[0]).setVal(rowcount, "a".getBytes());
                ((LongColumnVector) batch.cols[1]).fill(time + 1);
                ((LongColumnVector) batch.cols[2]).fill(1);
//                ((BytesColumnVector) batch.cols[3]).fill("1".getBytes());
                if (batch.size == 10) {
                    writer.addRowBatch(batch);
                    batch.reset();
                    System.out.println("wwwwww");
                }

            }
            writer.addRowBatch(batch);
            writer.close();
            long e = System.currentTimeMillis();
            System.out.println("cost=>" + (e - time));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
