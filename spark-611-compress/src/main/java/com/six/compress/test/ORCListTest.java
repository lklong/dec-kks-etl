package com.six.compress.test;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.orc.CompressionKind;
import org.apache.orc.OrcFile;
import org.apache.orc.TypeDescription;
import org.apache.orc.Writer;
import org.apache.orc.storage.ql.exec.vector.DoubleColumnVector;
import org.apache.orc.storage.ql.exec.vector.ListColumnVector;
import org.apache.orc.storage.ql.exec.vector.LongColumnVector;
import org.apache.orc.storage.ql.exec.vector.VectorizedRowBatch;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ORCListTest {

    public static void main(String[] args) throws Exception {
        //定义ORC数据结构，即表结构
        TypeDescription schema = TypeDescription.createStruct();
        schema.addField("time", TypeDescription.createLong());
        schema.addField("params1", TypeDescription.createList(TypeDescription.createFloat()));
        schema.addField("params2", TypeDescription.createList(TypeDescription.createBoolean()));
        Writer writer = null;
        try {
            //输出ORC文件本地绝对路径
            String lxw_orc1_file = "/home/hdfs/data/list.orc";
            Configuration conf = new Configuration();
            FileSystem.getLocal(conf);
            writer = OrcFile.createWriter(
                    new Path(lxw_orc1_file),
                    OrcFile.writerOptions(conf)
                            .setSchema(schema)
                            .compress(CompressionKind.NONE)
                            .version(OrcFile.Version.V_0_12)
            );

            Random random = new Random(1000000000);
            List<Float> list = new ArrayList<>();
            long start = System.currentTimeMillis();

            int size = 3000;
            int size2 = 24000;
            int max_row = 10000;

            VectorizedRowBatch batch = schema.createRowBatch(max_row);
            LongColumnVector time = (LongColumnVector) batch.cols[0];
            ListColumnVector paramVec1 = (ListColumnVector) batch.cols[1];
            ListColumnVector paramVec2 = (ListColumnVector) batch.cols[2];

            for (int i = 0; i < max_row; i++) {
                int rowcount = batch.size++;

                time.vector[i] = start + i;

                paramVec1.lengths[rowcount] = size;
                paramVec1.childCount = size;

                DoubleColumnVector ndsFloat = (DoubleColumnVector) paramVec1.child;
                ndsFloat.vector = new double[size];

                paramVec2.lengths[rowcount] = size;
                paramVec2.childCount = size;

                LongColumnVector ndslong = (LongColumnVector) paramVec2.child;
                ndslong.vector = new long[size2];

                for (int j = 0; j < size; j++) {
                    ndsFloat.vector[j] = random.nextFloat();
                }

                for (int j = 0; j < size2; j++) {
                    ndslong.vector[j] = 1;
                }

                if ((i + 1) % 1000 == 0) {
                    writer.addRowBatch(batch);
                    batch.reset();
                    long end = System.currentTimeMillis();
                    System.out.println("当前:第=>" + i + "条" + " cost=>" + (end - start) + "  avg=>" + (end - start) / i + "ms/条");
                }

            }
            writer.addRowBatch(batch);
            writer.close();
            long e = System.currentTimeMillis();
            System.out.println("cost=>" + (e - start) + "  avg=>" + (e - start) / max_row + "ms/条");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
