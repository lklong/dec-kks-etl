package com.six.compress.test;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.orc.CompressionKind;
import org.apache.orc.OrcFile;
import org.apache.orc.TypeDescription;
import org.apache.orc.Writer;
import org.apache.orc.storage.ql.exec.vector.LongColumnVector;
import org.apache.orc.storage.ql.exec.vector.VectorizedRowBatch;

public class ORCWriterRepeatTest {

    public static void main(String[] args) throws Exception {

        //定义ORC数据结构，即表结构
        TypeDescription schema = TypeDescription.createStruct();
        schema.addField("id", TypeDescription.createLong());
        schema.addField("time", TypeDescription.createLong());
        schema.addField("ff", TypeDescription.createBoolean());

        Writer writer = null;
        try {
            //输出ORC文件本地绝对路径
            String lxw_orc1_file = "/home/hdfs/test/test-"+System.currentTimeMillis()+".orc";
            Configuration conf = new Configuration();
            FileSystem.getLocal(conf);
            writer = OrcFile.createWriter(
                    new Path(lxw_orc1_file),
                    OrcFile.writerOptions(conf)
                            .setSchema(schema)
//                            .stripeSize(1024)
//                            .bufferSize(1024)
//                            .blockSize(1024)
                            .compress(CompressionKind.ZLIB)
                            .version(OrcFile.Version.V_0_12)
            );

            long start = System.currentTimeMillis();
            int max_row = 100;

            VectorizedRowBatch batch = schema.createRowBatch(max_row);

            for (int i = 0; i < max_row; i++) {
                int rowcount = batch.size++;
                long timev  =start + i;

                LongColumnVector id = (LongColumnVector)batch.cols[0];
                LongColumnVector time = (LongColumnVector)batch.cols[1];
                LongColumnVector ff = (LongColumnVector)batch.cols[2];

//                id.setRepeating(false);
//                time.setRepeating(false);

//                id.fill(batch.size);
//                time.fill(timev);

                id.vector[i] = batch.size;
                time.vector[i] = timev;
                ff.vector[i] = 1;

                if(rowcount+1 % 10 ==0){
                    writer.addRowBatch(batch);
//                    batch.reset();
                    System.out.println(batch);
                }

            }
            writer.addRowBatch(batch);
            writer.close();
            long e = System.currentTimeMillis();
            System.out.println("cost=>" + (e - start) / 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
