package compress;//import org.apache.orc.storage.ql.exec.vector.ListColumnVector;
//import org.apache.orc.storage.ql.exec.vector.LongColumnVector;
//import org.apache.orc.storage.ql.exec.vector.StructColumnVector;
//import org.apache.orc.storage.ql.exec.vector.VectorizedRowBatch;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hive.ql.exec.vector.ListColumnVector;
import org.apache.hadoop.hive.ql.exec.vector.LongColumnVector;
import org.apache.hadoop.hive.ql.exec.vector.StructColumnVector;
import org.apache.hadoop.hive.ql.exec.vector.VectorizedRowBatch;
import org.apache.orc.OrcFile;
import org.apache.orc.TypeDescription;
import org.apache.orc.Writer;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Random;

import static org.apache.orc.TypeDescription.*;

public class ORCListCompressTest {

    private static final TypeDescription SCHEMA = TypeDescription.createStruct()
            .addField("params", createList(
                    createStruct().addField("fly", createLong())
            ));

    @Test
    public void test() throws IOException {
        int max_row = 100;

        String filename = "/home/hdfs/data/test/list.orc";
        Configuration conf = new Configuration();
        Writer writer = OrcFile.createWriter(
                new Path(filename),
                OrcFile.writerOptions(conf)
                        .setSchema(SCHEMA));

        Random random = new Random(10000000);
        VectorizedRowBatch batch = SCHEMA.createRowBatch(max_row);
        ListColumnVector members = (ListColumnVector) batch.cols[0];

        for (int row = 0; row < max_row; row++) {

            members.lengths[row] = 20;
            members.childCount += members.lengths[row];
            members.child.ensureSize(members.childCount, members.offsets[row] != 0);

            for (int j = 0; j < 20; j++) {
                StructColumnVector ndsStruct = (StructColumnVector) members.child;
                ((LongColumnVector) ndsStruct.fields[0]).vector[(int) members.offsets[row] + j] = random.nextLong();
            }
        }
        writer.addRowBatch(batch);
        writer.close();

    }
}
