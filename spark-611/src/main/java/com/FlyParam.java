package com;

import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.List;

public class FlyParam implements Writable {

    private String channel;
    private long time;
    private List<Number> params;

    public FlyParam(){}
    public FlyParam(String channel, long time, List<Number> params) {
        this.channel = channel;
        this.time = time;
        this.params = params;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public List<Number> getParams() {
        return params;
    }

    public void setParams(List<Number> params) {
        this.params = params;
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        throw new UnsupportedOperationException("no write");
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        throw new UnsupportedOperationException("no read");
    }
}
