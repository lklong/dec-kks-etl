package com.dec.kks.etl.kafka;

import org.apache.kafka.common.TopicPartition;
import org.apache.spark.streaming.kafka010.OffsetRange;

public class KafkaOffsetManager {

    /**
     * 保存偏移量
     *
     * @param offsetRanges
     */
    public static void saveOffset(OffsetRange[] offsetRanges) {

    }

    /**
     * 保存偏移量
     *
     * @param topicPartition
     * @param offset
     */
    public static void saveOffset(TopicPartition topicPartition, long offset) {

    }

    /**
     * 获取偏移量
     * 判断偏移量是否为空，如果为空则从头开始（首次），否则从指定位置开始
     *
     * @param topicPartition
     * @return
     */
    public static long queryOffset(TopicPartition topicPartition) {

        return 0l;
    }

}
