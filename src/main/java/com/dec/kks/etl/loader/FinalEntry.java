package com.dec.kks.etl.loader;

import lombok.Data;

/**
 * Created by jie.meng on 2018/8/13.
 */
@Data
public class FinalEntry {

    private String srcCode;
    private String srcTime;
    private int srcDataQuality;
    private float srcValue;
    private String substdCode;
    private String stdCode;
    private String stdTime;
    private int stdDataQuality;
    private float stdValue;
    private String pltCode;
    private String setCode;
    private String tDay;
    private String tHour;

    public FinalEntry(String pltCode, String setCode, String tDay, String tHour) {
        this.pltCode = pltCode;
        this.setCode = setCode;
        this.tDay = tDay;
        this.tHour = tHour;
    }
}
