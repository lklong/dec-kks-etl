package com.dec.kks.etl.model;

import lombok.Data;

@Data
public class Coil {
    private double ecurrent;
    private int flow;
    private double rise;

}
