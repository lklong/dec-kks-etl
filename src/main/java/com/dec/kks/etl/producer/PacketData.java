package com.dec.kks.etl.producer;

import lombok.Data;

/**
 * <p/>
 * <li>Description:KKS kks内部数据解析</li>
 * <li>@author: liukailong <kailong.liu@cdcalabar.com> </li>
 * <li>Date: 18-4-4 上午9:35</li>
 * <li>@version: 2.0.0 </li>
 * <li>@since JDK 1.8 </li>
 */
@Data
public class PacketData {

    /**
     * "src_code":"测点原始编码1",   // 测点原始编码
     * "substd_code": "xxxx"         // 子系统编码
     * "src_desc": "xxxx",           // 测点描述
     * "src_value": 21.931957,       // 原始值
     * "src_time": 1517450744000,    // 原始时间
     * "src_data_quality": 192,      // 原始质量
     * "std_code": "xxxx",           // 标准编码
     * "std_time": 15174507446666,   // 校准时间
     * "std_value": 20.76,           // 校准值
     * "std_data_quality": 0,        // 校准质量
     * "set_code": "xxxxx"           // 机组编码
     */
    private String src_code;
    private String substd_code;
    private String src_desc;
    private Double src_value;
    private long src_time;
    private int src_data_quality;
    private String std_code;
    private long std_time;
    private Double std_value;
    private int std_data_quality;
    private String set_code;
}
