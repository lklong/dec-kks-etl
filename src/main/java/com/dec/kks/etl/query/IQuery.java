package com.dec.kks.etl.query;

public interface IQuery {

    void query() throws Exception;
}
