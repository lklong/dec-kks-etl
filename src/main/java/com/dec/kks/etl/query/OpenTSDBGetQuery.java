package com.dec.kks.etl.query;

import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.InputStream;
import java.net.URI;

public class OpenTSDBGetQuery implements IQuery {

    String url_put = "http://10.3.101.212:9995/api/put";
    String url_query = "http://10.3.101.212:9995/api/query";

    @Override
    public void query() throws Exception {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        CloseableHttpResponse response = null;
//        for (int i = 0; i < 100; i++) {
            try {
                URI uri = new URIBuilder()
                        .setScheme("http")
                        .setHost("localhost")
                        .setPort(4242)
                        .setPath("/api/query")
                        .setParameter("m", "sum:000000009431_src_value")
                        .setParameter("tags", "{\"plt_code\":\"T001\",\"set_code\":\"01\"}")
                        .setParameter("start", "2018/08/10-12:00:00")
                        .setParameter("end", "2018/08/12-14:00:25")
                        .build();
                HttpGet httpget = new HttpGet(uri);
                response = httpclient.execute(httpget);
                int statusCode = response.getStatusLine().getStatusCode();
                System.out.println(statusCode);
                InputStream stream = response.getEntity().getContent();
                String str = IOUtils.toString(stream, "utf-8");
                System.out.println(str);
            } finally {
                response.close();
            }
//        }

    }

    public static void main(String[] args) throws Exception {
        IQuery loader = new OpenTSDBGetQuery();
        loader.query();
    }
}
