package com.dec.kks.etl.query;

import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.InputStream;
import java.net.URI;

public class OpenTSDBPostQuery implements IQuery {

    @Override
    public void query() throws Exception {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        CloseableHttpResponse response = null;
        try {
            URI uri = new URIBuilder()
                    .setScheme("http")
                    .setHost("10.3.101.212")
                    .setPort(9995)
                    .setPath("/api/query")
                    .build();

            //language=json
            String json =
                    "{\n" +
                            " \"start\": \"1532400202000\",\n" +
                            " \"end\":  null,\n" +
                            " \"timezone\": null,\n" +
                            " \"options\": null,\n" +
                            " \"padding\": false,\n" +
                            " \"queries\": [{\n" +
                            "  \"aggregator\": \"sum\",\n" +
                            "  \"metric\": \"000000009412_src_value\",\n" +
                            "  \"tsuids\": null,\n" +
//                            "  \"downsample\": \"1h-avg\",\n" +
                            "  \"rate\": false,\n" +
                            "  \"filters\": [{\n" +
                            "   \"tagk\": \"plt_code\",\n" +
                            "   \"filter\": \"T001\",\n" +
                            "   \"group_by\": true,\n" +
                            "   \"type\": \"literal_or\"\n" +
                            "  }, {\n" +
                            "   \"tagk\": \"set_code\",\n" +
                            "   \"filter\": \"*\",\n" +
                            "   \"group_by\": true,\n" +
                            "   \"type\": \"wildcard\"\n" +
                            "  }],\n" +
                            "  \"rateOptions\": {\n" +
                            "   \"counter\": true,\n" +
                            "   \"counterMax\": 9223372036854775807,\n" +
                            "   \"resetValue\": 1,\n" +
                            "   \"dropResets\": false\n" +
                            "  }\n" +
                            " }],\n" +
                            " \"delete\": false,\n" +
                            " \"noAnnotations\": false,\n" +
                            " \"globalAnnotations\": false,\n" +
                            " \"showTSUIDs\": true,\n" +
                            " \"msResolution\": false,\n" +
                            " \"showQuery\": false,\n" +
                            " \"showStats\": false,\n" +
                            " \"showSummary\": false\n" +
                            "}";

            StringEntity entity = new StringEntity(json, ContentType.APPLICATION_JSON);
            HttpPost httppost = new HttpPost(uri);
            httppost.setEntity(entity);
            response = httpclient.execute(httppost);
            int statusCode = response.getStatusLine().getStatusCode();
            InputStream stream = response.getEntity().getContent();
            String str = IOUtils.toString(stream, "utf-8");
            System.out.println("status:" + statusCode + "->" + str);
        } finally {
            response.close();
        }
    }

    public static void main(String[] args) throws Exception {
        IQuery loader = new OpenTSDBPostQuery();
        for (int i = 0; i < 2; i++) {
            loader.query();
        }
    }
}
