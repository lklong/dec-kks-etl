package com.dec.hbase.pro;

import com.google.common.collect.Lists;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.util.JvmPauseMonitor;

import java.util.List;

public class JVMMonitorTest {

    public static void main(String[] args) throws Exception {
        new JvmPauseMonitor(new Configuration()).start();
        List<String> list = Lists.newArrayList();
        int i = 0;
        while (true) {
            list.add(String.valueOf(i++));
            Thread.sleep(1000);
            System.out.println("休眠结束");
        }
    }
}
