package com.dec.kks.etl.data.producer;

import org.apache.commons.lang3.time.DateFormatUtils;

public class DateTest {

    public static void main(String[] args) {
        // 提取日期
        long ct = System.currentTimeMillis();
        String date_yMd = DateFormatUtils.format(ct, "yyyy-MM-dd");
        // 提取小时
        String date_H = DateFormatUtils.format(ct, "HH");

        System.out.println(date_yMd+" "+date_H);
    }
}
